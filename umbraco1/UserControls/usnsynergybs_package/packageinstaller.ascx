﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="packageinstaller.ascx.cs" Inherits="USNSynergyBS_Package.usnsynergybs_package.packageinstaller" %>
<asp:Literal ID="litMessage" runat="server"></asp:Literal>
<asp:Panel ID="pnlSuccess" runat="server" Visible="false">
    <h3>uSkinned Synergy Starter Kit Successfully Installed</h3>
    <br/>
    <p>The installer has installed a website with dummy content to demonstrate all of the features of this theme.</p>
    <p>You can either modify the demo content or simply remove it once you are comfortable with how this theme works.</p>
    <asp:PlaceHolder ID="phExamineFailed" runat="server">
    <hr />
    <p>If you find that the search form on the website is not returning any content please follow the following steps:</p>
    <ol>
        <li>Go to the "Developer" section.</li>
        <li>Open the "Examine Management" tab.</li>
        <li>Click on the "ExternalIndexer" link.</li>
        <li>Click on the "Index info & tools" link.</li>
        <li>Click the "Rebuild index" button.</li>
    </ol>
    <p>This will rebuild the search index for your website, the search form will now return any content found that matches the entered text.</p>
    <hr />
    </asp:PlaceHolder>
    <p>Go to the "Content" section to manage the installed website.</p>
</asp:Panel>