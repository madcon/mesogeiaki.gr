﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Umbraco.Core.Models;
using createsend_dotnet;
using USN.BusinessLogic;
using USN.Models;
using Umbraco.Web;

namespace USN.Controllers
{
    /// <summary>
    /// Summary description for ContactFormSurfaceController
    /// </summary>
    public class ContactFormSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {

        public ActionResult Index(int NodeID, string PageType)
        {
            var model = new ContactFormViewModel();
            model.CurrentNodeID = NodeID;
            model.PageType = PageType;

            return PartialView("USNForms/USN_ContactForm", model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HandleContactSubmit(ContactFormViewModel model)
        {
            System.Threading.Thread.Sleep(1000);

            string returnValue = "";

            if (!ModelState.IsValid)
            {
                return JavaScript(String.Format("$(ContactError{0}).show();$(ContactError{0}).html('{1}');", model.CurrentNodeID, umbraco.library.GetDictionaryItem("USN Contact Form General Error")));
            }

            //Need to get NodeID from hidden field. CurrentPage does not work with Ajax.BeginForm
            var currentNode = Umbraco.TypedContent(model.CurrentNodeID);

            IPublishedContent homeNode = currentNode.AncestorOrSelf(1);
            var settingsFolder = Umbraco.TypedContent(homeNode.GetProperty("websiteConfigurationNode").Value);
            var globalSettings = settingsFolder.Children.Where(x => x.DocumentTypeAlias == "USNGlobalSettings").First();

            var mail = new USNMail.ContactFormMailVariables
            {
                FirstName = model.FirstName == null ? "" : model.FirstName,
                LastName = model.LastName == null ? "" : model.LastName,
                Email = model.Email == null ? "" : model.Email,
                Telephone = model.Telephone == null ? "" : model.Telephone,
                Message = model.Message == null ? "" : model.Message,
                To = currentNode.GetProperty("recipientEmailAddress").Value.ToString(),
                WebsiteName = globalSettings.GetProperty("websiteName").Value.ToString(),
                PageName = currentNode.Parent.Parent.Name
            };

            string errorMessage = "";

            if (!USNMail.SendContactFormMail(mail, out errorMessage))
            {
                return JavaScript(String.Format("$(ContactError{0}).show();$(ContactError{0}).html('<div class=\"info\"><p>{1}</p><p>{2}</p></div>');", model.CurrentNodeID, umbraco.library.GetDictionaryItem("USN Contact Form Mail Send Error"), errorMessage));
            }

            try
            {
                if (model.NewsletterSignup && globalSettings.HasValue("campaignMonitorAPIKey") &&
                    (globalSettings.HasValue("defaultCampaignMonitorSubscriberListID") || currentNode.HasValue("campaignMonitorSubscriberListID")))
                {
                    string subsciberListID = "";

                    if (currentNode.GetProperty("campaignMonitorSubscriberListID").Value.ToString() != String.Empty)
                        subsciberListID = currentNode.GetProperty("campaignMonitorSubscriberListID").Value.ToString();
                    else
                        subsciberListID = globalSettings.GetProperty("defaultCampaignMonitorSubscriberListID").Value.ToString();

                    AuthenticationDetails auth = new ApiKeyAuthenticationDetails(globalSettings.GetProperty("campaignMonitorAPIKey").Value.ToString());

                    Subscriber loSubscriber = new Subscriber(auth, subsciberListID);

                    List<SubscriberCustomField> customFields = new List<SubscriberCustomField>();

                    string lsSubscriberID = loSubscriber.Add(model.Email, model.FirstName + " " + model.LastName, customFields, false);
                }
            }
            catch (Exception ex)
            {
                return JavaScript(String.Format("$(ContactError{0}).show();$(ContactError{0}).html('<div class=\"info\"><p>{1}</p><p>{2}</p></div>');", model.CurrentNodeID, umbraco.library.GetDictionaryItem("USN Contact Form Signup Error"), ex.Message));
            }

            returnValue = String.Format("<div class=\"page_component alert alert-success alert-dismissible fade in\" role=\"alert\"><div class=\"info\">{0}</div></div>", currentNode.GetProperty("submissionMessage").Value.ToString());

            return Content(returnValue);
        }
    }
}