﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace USN.Models
{

    /// <summary>
    /// Summary description for NewsletterFormViewModel
    /// </summary>
    public class NewsletterFormViewModel
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")]
        public string Email { get; set; }

        public int CurrentNodeID { get; set; }

        public int ActualPageID { get; set; }

        public string PageType { get; set; }
    }

}