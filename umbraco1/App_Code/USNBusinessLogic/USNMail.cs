﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Logging;
using System.Web.UI.WebControls;

namespace USN.BusinessLogic
{

    /// <summary>
    /// Summary description for USNMail
    /// </summary>
    public class USNMail
    {
        public static bool SendContactFormMail(ContactFormMailVariables mailVariables, out string lsErrorMessage)
        {
            lsErrorMessage = "";

            try
            {
                //Create MailDefinition 
                MailDefinition md = new MailDefinition();
                string lsSendTo = "";

                //specify the location of template 
                md.BodyFileName = "/usn/emailtemplates/contactform.htm";
                md.IsBodyHtml = true;

                //Build replacement collection to replace fields in template 
                System.Collections.Specialized.ListDictionary replacements = new System.Collections.Specialized.ListDictionary();
                replacements.Add("<% formFirstName %>", mailVariables.FirstName);
                replacements.Add("<% formLastName %>", mailVariables.LastName);
                replacements.Add("<% formEmail %>", mailVariables.Email);
                replacements.Add("<% formPhone %>", mailVariables.Telephone);
                replacements.Add("<% formMessage %>", umbraco.library.ReplaceLineBreaks(mailVariables.Message));
                replacements.Add("<% WebsitePage %>", mailVariables.PageName);
                replacements.Add("<% WebsiteName %>", mailVariables.WebsiteName);

                lsSendTo = mailVariables.To;

                //now create mail message using the mail definition object 
                System.Net.Mail.MailMessage msg = md.CreateMailMessage(lsSendTo, replacements, new System.Web.UI.Control());
                msg.ReplyTo = new System.Net.Mail.MailAddress(mailVariables.Email);
                msg.Subject = mailVariables.WebsiteName + " Website: " + mailVariables.PageName + " Page Enquiry";

                //this uses SmtpClient in 2.0 to send email, this can be configured in web.config file.
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                smtp.Send(msg);

                return true;
            }
            catch (Exception ex)
            {
                lsErrorMessage = ex.Message;
                LogHelper.Error<USNMail>("Error creating or sending Email.", ex);
            }

            return false;
        }

        public class ContactFormMailVariables
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Telephone { get; set; }
            public string Message { get; set; }
            public string To { get; set; }
            public string WebsiteName { get; set; }
            public string PageName { get; set; }
        }

        
    }
}